import random

from particle import Particle

class Particle_list:
    MATRIX_SIZE = 8

    def __init__(self, matrice_size):
        self.MATRIX_SIZE = matrice_size
        self.p_list = [-1]*self.MATRIX_SIZE*self.MATRIX_SIZE  #remplie la liste de la valeur -1 (pour une place vide)

    def append(self, particle): #ajoute une particule a la liste ssi la position n'est pas deja occupée
        if not self.positionIsAlreadyTaken(particle.x, particle.y):
            self.p_list[particle.x + particle.y*self.MATRIX_SIZE] = particle
    
    def addParticleRandomPos(self):
        new_pos = self.getRandomFreePosition()
        self.append(Particle(new_pos["x"], new_pos["y"]))

    def listIsFull(self):   #test si la liste de particules est pleine (toutes les valeurs differentes de -1)
        for p in self.p_list:
            if p == -1:
                return False
        return True

    def positionIsAlreadyTaken(self, x, y): #test si la position x,y de la matrice est deja occupée
        if self.p_list[x + y*self.MATRIX_SIZE] != -1:
            return True
        return False

    def getRandomFreePosition(self):    #renvoie un dictionnaire contenant les coordonnées x et y d'une position libre de la matrice 
        if not self.listIsFull():
            for i in range(self.MATRIX_SIZE):
                newX, newY = random.randint(0,self.MATRIX_SIZE-1), random.randint(0, self.MATRIX_SIZE-1)

                if not self.positionIsAlreadyTaken(newX, newY):
                    return {"x":newX, "y":newY}
