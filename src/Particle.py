

class Particle :
    x = 0
    y = 0
    
    color = "rgb"
    state = 0
    
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def setX(self, value):
        self.x = value
        
    def setY(self, value):
        self.y = value

    def getX(self):
        return self.x
    
    def getY(self):
        return self.y
    
    #reste a determiner les valeurs de l'accelerometre (x, y, z) par rapport aux valeurs de la matrice (x, y, z) car potentiellement les deux ne "pointent" pas dans la meme direction

    